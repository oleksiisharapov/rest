package com.blinddog.restclient

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EmployeeViewModel(Repository(ApiClient.createService(DummyApi::class.java))) as T
    }
}