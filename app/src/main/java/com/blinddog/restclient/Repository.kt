package com.blinddog.restclient

import androidx.lifecycle.MutableLiveData

class Repository(private val api: DummyApi) {

    private val mutableDataset = MutableLiveData<List<Employee>>()

    suspend fun getEmployees(): MutableLiveData<List<Employee>>{

        val resultList = ApiClient.requestDataObject{
            api.getEmployees()
        }

        mutableDataset.postValue(resultList)
        return mutableDataset
    }

}