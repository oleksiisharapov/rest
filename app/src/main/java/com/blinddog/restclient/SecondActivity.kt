package com.blinddog.restclient

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.blinddog.restclient.databinding.SecondActivityBinding

class SecondActivity: AppCompatActivity(){


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: SecondActivityBinding = DataBindingUtil.setContentView(this, R.layout.second_activity)

        binding.textValue = "MyFirstDataBinding"
        binding.lifecycleOwner = this

    }


}