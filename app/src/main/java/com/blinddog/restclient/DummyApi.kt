package com.blinddog.restclient

import retrofit2.Response
import retrofit2.http.GET

interface  DummyApi{

    @GET("employees")
    suspend fun getEmployees(): Response<ApiClient.DataResponse<List<Employee>>>


}