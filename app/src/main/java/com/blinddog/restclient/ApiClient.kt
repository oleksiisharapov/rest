package com.blinddog.restclient

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@SuppressLint("StaticFieldLeak")
object ApiClient {

    data class DataResponse<T : Any>(val status: String, val data: T)

    private lateinit var context: Context


    private val retrofit: Retrofit by lazy {
        val builder = Retrofit.Builder()
            .baseUrl("http://dummy.restapiexample.com/api/v1/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .client(OkHttpClient.Builder().addInterceptor(NetworkStateInterceptor(context)).build())
        builder.build()
    }

    fun <T> createService(apiClass: Class<T>): T {
        return retrofit.create(apiClass)
    }


    fun init(context: Context){
        this.context = context
    }

    suspend fun <T : Any> requestDataObject(call: suspend () -> Response<DataResponse<T>>): T {
        lateinit var response: Response<DataResponse<T>>
        try {
            response = call.invoke()
        } catch (e: Exception) {
            Log.d("Exception11", e.message ?: "empty")
        }
        if (response.isSuccessful) {
            return response.body()!!.data
        } else {
            throw Exception("Request is empty")
        }
    }

}