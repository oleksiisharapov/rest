package com.blinddog.restclient

import android.app.Application

class RestClientApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        ApiClient.init(this)
    }

}