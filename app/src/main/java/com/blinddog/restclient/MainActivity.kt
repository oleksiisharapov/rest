package com.blinddog.restclient

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blinddog.restclient.databinding.ActivityMainBinding
import com.blinddog.restclient.databinding.ItemRowBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var viewModel: EmployeeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        val viewModelFactory = ViewModelFactory()

        viewModel = ViewModelProvider(this, viewModelFactory).get(EmployeeViewModel::class.java)


        binding.viewModel = viewModel
        binding.requestButton.setOnClickListener(this)

        recyclerView.layoutManager = LinearLayoutManager(this)
        val adapter = EmployeeAdapter(viewModel.response.value!!, EmployeeAdapter.RecyclerViewItemClickListener {
            Toast.makeText(this, "${it.employee_name} clicked", Toast.LENGTH_LONG).show()
        })
        recyclerView.adapter = adapter

        viewModel.response.observe(this, Observer{
          if(it != null) {
              adapter.itemList = it
              adapter.notifyDataSetChanged()
          }
        })

        viewModel._showProgress.observe(this, Observer{
            if(it){
                progressBar.visibility = View.VISIBLE
            }else{
                progressBar.visibility = View.GONE
            }
        })

    }

    override fun onClick(p0: View?) {
        viewModel.fetchEmployees()
    }


    class EmployeeAdapter(var itemList: List<Employee>, val clickListener: RecyclerViewItemClickListener) : RecyclerView.Adapter<EmployeeAdapter.EmployeeVH>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeVH {

            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemRowBinding.inflate(inflater, parent, false)
            return EmployeeVH(binding)
        }

        override fun getItemCount(): Int {
            return itemList.size
        }

        override fun onBindViewHolder(holder: EmployeeVH, position: Int) {
            holder.bind(itemList[position], clickListener)
        }

        inner class EmployeeVH(val binding: ItemRowBinding) : RecyclerView.ViewHolder(binding.root) {
            fun bind(item: Employee, clickListener: RecyclerViewItemClickListener) {
                binding.employee = item
                binding.clickListener = clickListener
                binding.executePendingBindings()
            }
        }


        class RecyclerViewItemClickListener(val listener: (employee: Employee) -> Unit) {
            fun onClick(employee: Employee) {
                listener(employee)
            }
        }
    }



}

