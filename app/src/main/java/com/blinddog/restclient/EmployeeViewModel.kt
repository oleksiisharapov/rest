package com.blinddog.restclient

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EmployeeViewModel(private val repository: Repository) : ViewModel(){

    private var responseLiveData = MutableLiveData<List<Employee>>()

    private var showProgress =  MutableLiveData<Boolean>()


    init{
        responseLiveData.value = listOf(Employee(0, "null", 20, 30, ""))
        showProgress.value = false
    }

    val response: LiveData<List<Employee>>
        get() = responseLiveData

    val _showProgress:  LiveData<Boolean>
        get() = showProgress


    fun fetchEmployees(){
        viewModelScope.launch{
            showProgress.postValue(true)
            responseLiveData.value = withContext(IO){   repository.getEmployees().value}
            showProgress.postValue(false)
        }
    }



}